Simple Database Adapter (SDBA)
==============================

In English
----------

What's this? I'd like to introduce you my adapter for MySQLi — Simple Database Adapter (or just SDBA). It can be useful
to make your life a bit easier. Use this library if you want something lightweight and simple. Don't use SDBA if you're
going to execute complex queries or take advantages of migrations. Also, pay your attention to the fact that you're able
to work with only one database using SDBA! It's a very, very simple library for unpretentious developers! OK, I hope
you completely understand it. So, we're ready to move on further.  

First of all, let me explain how to set configurations and credentials for your database.  
The library doesn't use the constructor at all. You don't need to create an instance! Where should you set credentials
and settings then? SDBA reads the file named `env.cfg` in the root directory of your website. When you call any method
(almost any) at the first time, the library creates an internal instance of the MySQLi object.  
What about the structure of that file? Well, `env.cfg` is just a simple PHP file. You can see an example named
`env.cfg.example` and stored somewhere alongside with this manual. All it does is to return an array with only one key: `db`.
In its turn, a value of `db` must be an array too. Keys of the latter array are: `host`, `login`, `password`, `database`,
`timezone`. I hope they're pretty obvious.  
The last thing I want to note is that you can change the default name for the file (or all behavior entirely) by modifying
the method `Start()`.  

Well, let's go through exceptions and proceed to the list of methods. Here we have a very simple mechanism. If you have
an error in your query, you get the standard exception: `mysqli_sql_error`. That's all!  


### Data fetching

Here we'll consider one, but the most complex function:  
`Select($columns, $table, array $filter=null, $sorting=null, $limit=null, $unique=false)`  
It takes 6 parameters, 2 of them are mandatory:  

1. `$columns` — determines which fields we're going to fetch from the database. It can be either a string or an
array of strings. Each string is the name of a field.  
2. `$table` — determines the name of a table where we're going to fetch the data from. Just a string.  
3. `$filter` — if know SQL well, you must be already got it that here you should describe conditions to filter the outcome.  
Possible values are either *null* (by default) or an array. We'll consider the structure of the latter in detail later.  
4. `$sorting` — determines a sorting order. Can be either a string (then all rows will be sorted by this field
in ascending order) or an array of strings (or *null* as set by default). In the latter case, each string also may be
a key (but it isn't necessary). If you assign the value `SDBA::DESC_ORDER` (or just *true*) to a key, it will be sorted
in descending order. We'll cover it in more details later.  
5. `$limit` — if it's not *null* (by default), then it's either a number determining the maximum count of rows you want
to fetch or an array of two numbers (the first one will be treated as an offset, the second — as a limitation).  
6. `$unique` — it's a boolean. If it's *true*, all rows will be unique. Otherwise (by default), it's possible to fetch
duplicate rows.  

Let's get back to `$filter` and consider it in detail. The simplest case is when we have an array of pairs *key-value*.
Keys are the names of fields. If you perform such query, you get all rows where the values of fields are equal to your
values in the array.  
What about other comparison operators except equality? Yes, the library supports them. Just pass a string with operator
into the array **before** operands. Unclear, huh? Just wait a bit, and examine examples below mindfully.  
All supported comparison operators are `=` (by default), `<>` and `!=`, `<=`, `<`, `>=`, `>`, `<=>`.  

Finally, the example:  
```php
<?php

// Fetches the names of all users.
$users = SDBA::Select("name", "users");

// Now we want to get only the first user using the filtration.
$user1 = SDBA::Select("name", "users", ['id' => 1]);

// Users with ID between 1 and 10. We're using sorting by ID in ascending order,
// and we want to get not more than 10 rows.
$users1_10 = SDBA::Select("name", "users", null, "id", 10);

// Let's set the offset too and get users between 10 and 20.
$users10_20 = SDBA::Select("name", "users", null, "id", [10, 10]);

// To get the last 10 users, let's sort the table in descending order.
$lastUsers = SDBA::Select("name", "users", null, ['id' => SDBA::DESC_ORDER], 10);

// Let's fetch all registration dates without duplicates.
$uniqueUsers = SDBA::Select("registration_date", "users", null, null, null, true);

// The last and the most complex example: we want to get the ID, name and registration date of all users
// who registered less than a month ago and sort them by registration date in descending order.
// Then we'll sort users with the same registration date in alphabetical order.
$newUsers = SDBA::Select(
    ['id', 'name', 'registration_date'],
    "users",
    [
        '>=',
        'registration_date' => date("Y-m-j H:i:s", time() - 2592000)
    ],
    [
        'registration_date' => SDBA::DESC_ORDER,
        'name'
    ]
);


// Now we can use standard methods of MySQLi.
$userList = [];
while ($userList[] = $newUsers->fetch_assoc());
```


### Data modification

We have 3 basic methods and 2 additional. Let's look at them closer one by one.  

#### Insertion

`Insert($table, array $data)`  

It takes two arguments:  

* `$table` — the name of a table, where we're going to insert data into.  
* `$data` — an array with key-value pairs, where keys are the names of fields and values are data for them.  

Pretty simple, isn't it? You can't insert a row omitting the names of fields. This behavior lets you write more
obvious, clear and understandable code.  

```php
<?php

SDBA::Insert("users", [
    'name'  => 'Leonid Kozarin',
    'email' => 'kozalo@nekochan.ru'
]);
```

#### Updating

`Update($table, array $changes, array $filter=null)`  

It takes the following arguments:  

* `$table` — the name of a table which we're going to update.  
* `$changes` — an array where keys are the names of modifying fields and values are new data for them.  
* `$filter` (optional) — if not *null*, it's an array determining which rows will be modified. We discussed
about that in detail when spoke about data fetching.  

```php
<?php

// Let's make the first user be an administrator's account.
SDBA::Update("users", [
    'name'  => 'Admin',
    'email' => 'admin@nekochan.ru'
], [
    'id' => 1
]);
```


#### Deletion

`Delete($table, array $filter=null)`  

It takes the following arguments:  

* `$table` — the name of a table where we're going to delete data from.  
* `$filter` (optional) — if not *null*, it's an array determining which rows will be deleted. We discussed
about that in detail when spoke about data fetching.  

```php
<?php

// Let's be bad boys and delete all users except the first one.
SDBA::Delete("users", [
    '>',
    'id' => 1
]);
```

#### Copying

`Copy($source, $destination, array $filter=null)`  

This method copies rows from one table into another. Both tables must have the same structure! It takes the following arguments:  

* `$source` — the name of a table, where we're going to take rows from.  
* `$destination` — the name of a table, where we're going to copy data into.  
* `$filter` (optional) — if not *null*, it's an array determining which rows will be copied. We discussed
about that in detail when spoke about data fetching.   

```php
<?php

// We're going to move the first ten users from one table into another.
$filter = [
  '<=',
  'id' => 10
];

// Let's copy them into a VIP table.
SDBA::Copy("users", "vip_users", $filter);

// And then delete them from the table for commoners.
SDBA::Delete("users", $filter);
```

#### Moving

`Move($source, $destination, array $filter=null)`  

This method moves rows from one table into another. Both tables must have the same structure! It takes the following arguments:  

* `$source` — the name of a table, where we're going to take rows from.  
* `$destination` — the name of a table, where we're going to copy data into.  
* `$filter` (optional) — if not *null*, it's an array determining which rows will be copied. We discussed
about that in detail when spoke about data fetching.    

```php
<?php

// We're going to move the first ten users into the VIP table again.
SDBA::Move("users", "vip_users", [
    '<=',
    'id' => 10
]);

// But in this case, we don't need to delete any rows!
```


### Complex queries

Please, use all capabilities of your DBMS: views, procedures and functions, triggers. Try to write as much
logic as possible at the side of the database. I appeal you to keep your client-side code clean and simple!  

Nevertheless, if you have to execute some complex query, you're able to call the method
`CustomQuery($query, $typesMask=null, $values=null)`.  
It takes the following arguments listed below:  

* `$query` — any valid MySQL query, in which all values replaced by question marks (see the example below).  
* `$typesMask` — if your query doesn't use concrete values at all, just omit this and the next parameters. But in most
complex cases, you must declare types of your values here. It's used as a protection from SQL injections.
All types must be listed in a row as characters (`i` - integer, `d` - double, `s` - string). See the example below for more information.  
* `$values` — used in pair with the previous parameter. It may be either a single value or an array of values.
Types of the values must correspond with the types described with the previous argument!  

```php
<?php

// Yeah, I couldn't think of anything better than this simple example, in which we're updating the data again.
SDBA::CustomQuery(
    "UPDATE `users` SET `name`=?, `email`=? WHERE `id`=?",
    "ssi",
    ['Admin', 'admin@nekochan.ru', 1]
);
```

#### Prepared queries

Let's talk a little more about how complex queries work behind the scenes. `ComplexQuery()` uses two other public methods:
`PrepareQuery($query, $typesMask=null, &$bindings=null)` и `ExecutePreparedQuery()`. You also can (and even *should*) use
them if you have one SQL query and a whole set of different values for it. Optimisation! It's good for performance to
compile the query one time and then only change concrete values.  

To compile a query, use the method `PrepareQuery()`. The first and second arguments have the same names as parameters of
`ComplexQuery()` and they're completely equal to them. What does `&$bindings` mean? Look, you must declare variables for
future values before preparing a query. Then you just pass these variables as *bindings* in an array (just pass a variable
itself if you have only one value in the query). After that, all you need to do are:  

1. Assign appropriate values to the variables.  
2. Call `ExecutePreparedQuery()`.  
3. Repeat it for each set of values.  

That's all!  


### Transactions

Of course, SDBA has methods for working with transactions. There are two methods:  

1. `StartTransaction()` to, obviously, begin a transaction.  
2. `Commit()` to save changes in a database.  

If an error occurs in any query between them, all transaction will be canceled.  


### Other methods

Here we're consider the last 3 methods:  

1. `ChangeTimezone($offset)` — adds `$offset` hours to the current timezone.  
2. `GetLastInsertedId()` — it's a very useful method! It returns the last generated value of AUTO_INCREMENT.
Note that it should be called immediately after performing a query (in particular, before calling `Commit()`)!  
3. `GetLastQuery()` — this method is supposed for widely using in debugging. It returns the last generated SQL query
as a string.  


*Author: Leonid Kozarin (<kozalo@yandex.ru>)*  
*© Kozalo.Ru, 2017*
