<?php
namespace ru\kozalo;

use mysqli;
use mysqli_result;
use mysqli_sql_exception;

/**
 * Simple Database Adapter (just another adapter class for MySQLi)
 *
 * Credentials and settings for a database to connect must be in the root directory of your website named 'env.cfg'.
 *
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 * @package ru\kozalo
 * @license MIT
 * @uses mysqli
 */
class SDBA {
    /**
     * mysqli
     *
     * Contains a MySQLi object.
     *
     * @var mysqli
     */
    private static $mysqli;

    /**
     * lastQuery
     *
     * Contains the last query used by the class.
     * Used for logging and debugging purposes. You can use the GetLastQuery() method to get its value.
     *
     * @var string
     * @see GetLastQuery()
     */
    private static $lastQuery;

    /**
     * preparedQuery
     *
     * Used to store a prepared query before it will be executed.
     *
     * @var \mysqli_stmt
     * @see PrepareQuery()
     * @see ExecutePreparedQuery()
     */
    private static $preparedQuery;

    /**
     * committed
     *
     * If you try to commit a unbegun transaction, you just get *false*. Thank this variable.
     *
     * @var bool
     * @see StartTransaction()
     * @see Commit()
     */
    private static $committed = true;

    /**
     * @const ASC_ORDER Use this constant instead of non-informative *false* when you want to get a result in ascending order.
     * @see BuildSelectQuery()
     */
    const ASC_ORDER = false;

    /**
     * @const DESC_ORDER Use this constant instead of non-informative *true* when you want to get a result in descending order.
     * @see BuildSelectQuery()
     */
    const DESC_ORDER = true;


    /**
     * Start
     *
     * Ensures if there is a database connection.
     * If there is no MySQLi object, creates it and set some necessary options.
     *
     * @param void
     * @return void
     * @throws mysqli_sql_exception
     */
    private static function Start()
    {
        if (isset(self::$mysqli) && (self::$mysqli instanceof mysqli)) return;

        $settings = include('env.cfg');
        $settings = $settings['db'];

        if (!mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT))
            throw new mysqli_sql_exception("Cannot set a report mode for the MySQLi driver.");

        self::$mysqli = new mysqli($settings["host"], $settings["login"], $settings["password"], $settings["database"]);
        self::$mysqli->set_charset("utf8");
        self::ChangeTimezone($settings["timezone"]);
    }


    /**
     * Query
     *
     * Passes the query to the mysqli::query() method.
     * Additionally, assign the query string to the $lastQuery field for logging purposes.
     *
     * @param string $query
     * @return bool|mysqli_result
     */
    private static function Query($query)
    {
        self::$lastQuery = $query;
        return self::$mysqli->query($query);
    }


    /**
     * MultiQuery
     *
     * Passes the query to the mysqli::multi_query() method.
     * Additionally, assign the query string to the $lastQuery field for logging purposes.
     * Gets some queries in one string separated with semicolon.
     *
     * @param string $query
     * @return bool
     */
    private static function MultiQuery($query)
    {
        self::$lastQuery = $query;
        return self::$mysqli->multi_query($query);
    }


    /**
     * BuildEscapedWhereClause
     *
     * Helper method. Returns a where clause of the future query.
     * If the input is an array, the method parses it, escapes strings and returns the result.
     * Otherwise, returns the empty string.
     *
     * Array must be like this:
     * [
     *    'name' => 'Piper Halliwell',
     *    'AND',
     *    '>=',
     *    'id' => '2',
     * ]
     *
     * @param mixed $where
     * @return string
     */
    private static function BuildEscapedWhereClause($where)
    {
        if (is_array($where))
        {
            $whereQuery = 'WHERE';
            $operator = '=';

            foreach ($where as $key => $value)
            {
                $value = self::$mysqli->real_escape_string($value);

                if (is_numeric($key))
                {
                    if ($value == '='
                     || $value == '<>'
                     || $value == '!='
                     || $value == '<='
                     || $value == '<'
                     || $value == '>='
                     || $value == '>'
                     || $value == '<=>')
                    {
                        $operator = $value;
                    }
                    else
                        $whereQuery .= " $value";
                }
                else {
                    $key = self::$mysqli->real_escape_string($key);
                    $whereQuery .= " `$key`$operator'$value'";
                }
            }
            $where = $whereQuery;
        }
        else
            $where = '';

        return $where;
    }


    /**
     * BuildSelectQuery
     *
     * Build a complete select query from separate parts.
     * Parses the parameters, escapes strings and returns the query.
     *
     * @param array|string  $what                   Either the name of a field, or an array of names.
     * @param string        $from                   The name of a table to select from.
     * @param array|string  $where                  Depending on the $complexWhereClause parameter, either uses a raw string, or use the BuildEscapedWhereClause() method.
     * @param array|string  $orderBy                It can be just a string with the name of the field or an array of such strings. Also, you can use the name as a key and assign SDBA::DESC_ORDER to it as a value. In this case, the field will be sorted in descending order.
     * @param array|int     $limit                  Either the number of required rows, or the array of the number and offset.
     * @param bool          $unique                 If true, the result will consist of only unique rows.
     * @return string
     * @see self::BuildEscapedWhereClause()
     * @uses self::BuildEscapedWhereClause()
     */
    private static function BuildSelectQuery($what, $from, $where=null, $orderBy=null, $limit=null, $unique=false)
    {
        $what = self::$mysqli->real_escape_string(implode('`,`', (array)$what));
        $from = self::$mysqli->real_escape_string($from);
        $where = self::BuildEscapedWhereClause($where);

        if (is_array($orderBy))
        {
            $orderByArr = [];

            foreach ($orderBy as $key => $value) {
                if (is_numeric($key)) {
                    $field = self::$mysqli->real_escape_string($value);
                    $ascDesc = "ASC";
                } else {
                    $field = self::$mysqli->real_escape_string($key);
                    $ascDesc = ($value) ? "DESC" : "ASC";
                }

                $orderByArr[] = "`$field` $ascDesc";
            }

            $orderBy = "ORDER BY " . implode(", ", $orderByArr);
        }
        elseif (!empty($orderBy))
        {
            $orderBy = self::$mysqli->real_escape_string($orderBy);
            $orderBy = "ORDER BY `$orderBy`";
        }
        else
            $orderBy = '';

        if (is_array($limit) && (count($limit) >= 2))
            $limit = 'LIMIT ' . (int)$limit[0] . ', ' . (int)$limit[1];
        elseif (is_numeric($limit))
            $limit = "LIMIT $limit";
        else
            $limit = '';

        if ($unique)
            $unique = 'DISTINCT';
        else
            $unique = '';

        return "SELECT $unique `$what` FROM `$from` $where $orderBy $limit";
    }


    /**
     * BuildInsertQuery
     *
     * Build a complete insert query from separate parts.
     * Parses the parameters, escapes strings and returns the query.
     *
     * @param string $where The name of a table to insert into.
     * @param array $data An array like: ['field_name'=>'value'].
     * @return string
     */
    private static function BuildInsertQuery($where, array $data)
    {
        $where = self::$mysqli->real_escape_string($where);

        $fields = array_keys($data);
        $values = array_values($data);

        $fieldsStr = '';
        $valuesStr = '';
        for ($i = 0; $i < count($data); $i++)
        {
            $fieldsStr .= "`" . self::$mysqli->real_escape_string($fields[$i]) . "`, ";
            $valuesStr .= "'" . self::$mysqli->real_escape_string($values[$i]) . "', ";
        }
        $fields = mb_substr($fieldsStr, 0, -2);
        $values = mb_substr($valuesStr, 0, -2);

        return "INSERT INTO `$where` ($fields) VALUES ($values)";
    }


    /**
     * BuildUpdateQuery
     *
     * Build a complete update query from separate parts.
     * Parses the parameters, escapes strings and returns the query.
     *
     * @param string $what The name of a table to update.
     * @param array $updates An array like: ['field_name'=>'value'].
     * @param array $where See the BuildEscapedWhereClause() method.
     * @return string
     * @see self::BuildEscapedWhereClause()
     * @uses self::BuildEscapedWhereClause()
     */
    private static function BuildUpdateQuery($what, array $updates, array $where=null)
    {
        $what = self::$mysqli->real_escape_string($what);
        $where = self::BuildEscapedWhereClause($where);

        $updatesStr = '';
        foreach ($updates as $key => $value)
        {
            $key = self::$mysqli->real_escape_string($key);
            $value = self::$mysqli->real_escape_string($value);
            $updatesStr .= "`$key`='$value', ";
        }
        $updates = mb_substr($updatesStr, 0, count($updatesStr)-3);

        return "UPDATE `$what` SET $updates $where";
    }

    /**
     * BuildDeleteQuery
     *
     * Build a complete delete query from separate parts.
     * Parses the parameters, escapes strings and returns the query.
     *
     * @param string $from The name of a table to delete from.
     * @param array $where See the BuildEscapedWhereClause() method.
     * @return string
     * @see self::BuildEscapedWhereClause()
     * @uses self::BuildEscapedWhereClause()
     */
    private static function BuildDeleteQuery($from, array $where=null)
    {
        $from = self::$mysqli->real_escape_string($from);
        $where = self::BuildEscapedWhereClause($where);

        return "DELETE FROM `$from` $where";
    }


    /**
     * BuildCopyQuery
     *
     * Builds a string consisting of INSERT and SELECT queries.
     * Parses the parameters, escapes strings and returns the query.
     *
     * @param string $from The name of a table to select from.
     * @param string $to The name of a table to insert into.
     * @param array $where See the BuildEscapedWhereClause() method.
     * @return string
     * @see self::BuildEscapedWhereClause()
     * @uses self::BuildEscapedWhereClause()
     */
    private static function BuildCopyQuery($from, $to, array $where=null)
    {
        $from = self::$mysqli->real_escape_string($from);
        $to = self::$mysqli->real_escape_string($to);
        $where = self::BuildEscapedWhereClause($where);

        $fromInfo = self::Query("SHOW COLUMNS FROM `$from`");

        $fields = [];
        while ($column = $fromInfo->fetch_assoc())
            $fields[] = $column['Field'];

        $fields = implode('`, `', $fields);

        return "INSERT INTO `$to` (`$fields`) SELECT `$fields` FROM $from $where";
    }


    /**
     * ChangeTimezone
     *
     * @param int $offset in hours.
     * @return bool|mysqli_result
     */
    public static function ChangeTimezone($offset)
    {
        if ($offset < 0) return false;
        if ($offset < 10) $offset = '0' . $offset;

        return self::Query("SET time_zone = '+$offset:00'");
    }


    /**
     * Select
     *
     * Selects some data from the database.
     * Parses the parameters, builds a query, passes it to the database and returns the result.
     *
     * $piper=DBManager::Select(['power', 'age'], 'sisters_witches',
     * [
     *    'name' => 'Piper Halliwell',
     *    'AND',
     *    '>=',
     *    'id' => '2',
     * ]);
     *
     * @param array|string  $columns   Either the name of a field, or an array of names.
     * @param string        $table     The name of a table to select from.
     * @param array         $filter    See the description.
     * @param array|string  $sorting   Either the name of a field, or the array of this name and a bool. If the bool is true, the result of the query will be sorted in descending order.
     * @param array|int     $limit     Either the number of required rows, or the array of the number and offset.
     * @param bool          $unique    If true, the result will consist of only unique rows.
     * @return mysqli_result
     * @throws mysqli_sql_exception
     * @uses self::BuildSelectQuery()
     * @uses self::Query()
     */
    public static function Select($columns, $table, array $filter=null, $sorting=null, $limit=null, $unique=false)
    {
        self::Start();
        $query = self::BuildSelectQuery($columns, $table, $filter, $sorting, $limit, $unique);
        return self::Query($query);
    }


    /**
     * Insert
     *
     * Insert some data into the database.
     * Parses the parameters, builds the query and passes it to the database.
     *
     * @param string $table The name of a table to insert into.
     * @param array $data An array like: ['field_name'=>'value'].
     * @throws mysqli_sql_exception
     * @uses self::BuildInsertQuery()
     * @uses self::Query()
     */
    public static function Insert($table, array $data)
    {
        self::Start();
        $query = self::BuildInsertQuery($table, $data);
        self::Query($query);
    }


    /**
     * GetLastInsertedId
     *
     * Returns the last automatically generated id.
     *
     * @see http://php.net/manual/en/mysqli.insert-id.php mysqli::insert_id
     * @return int
     * @uses self::$mysqli
     */
    public static function GetLastInsertedId()
    {
        self::Start();
        return self::$mysqli->insert_id;
    }


    /**
     * Update
     *
     * Update some data in the database.
     * Parses the parameters, builds the query and passes it to the database.
     *
     * Array must be like this:
     * [
     *    'name' => 'Piper Halliwell',
     *    'AND',
     *    '>=',
     *    'id' => '2',
     * ]
     *
     * @param string $table The name of a table to update.
     * @param array $changes An array like: ['field_name'=>'value'].
     * @param array $filter See the BuildEscapedWhereClause() method for more information.
     * @throws mysqli_sql_exception
     * @uses self::BuildUpdateQuery()
     * @uses self::Query()
     */
    public static function Update($table, array $changes, array $filter=null)
    {
        self::Start();
        $query = self::BuildUpdateQuery($table, $changes, $filter);
        self::Query($query);
    }


    /**
     * Delete
     *
     * Delete some data from the database.
     * Parses the parameters, builds the query and passes it to the database.
     *
     * Array must be like this:
     * [
     *    'name' => 'Piper Halliwell',
     *    'AND',
     *    '>=',
     *    'id' => '2',
     * ]
     *
     * @param string $table The name of a table to delete from.
     * @param array $filter See the description.
     * @throws mysqli_sql_exception
     * @uses self::BuildDeleteQuery()
     * @uses self::Query()
     */
    public static function Delete($table, array $filter=null)
    {
        self::Start();
        $query = self::BuildDeleteQuery($table, $filter);
        self::Query($query);
    }


    /**
     * Copy
     *
     * Copies rows from one table into another.
     * Parses the parameters, builds the query and passes it to the database.
     *
     * Array must be like this:
     * [
     *    'name' => 'Piper Halliwell',
     *    'AND',
     *    '>=',
     *    'id' => '2',
     * ]
     *
     * @param string $source The name of a table to move from.
     * @param string $destination The name of a table to move into.
     * @param array $filter See the description
     * @throws mysqli_sql_exception
     * @uses self::BuildCopyQuery()
     * @uses self::Query()
     */
    public static function Copy($source, $destination, array $filter=null)
    {
        self::Start();
        $copyQuery = self::BuildCopyQuery($source, $destination, $filter);
        self::Query($copyQuery);
    }


    /**
     * Move
     *
     * Moves rows from one table into another.
     * Parses the parameters, builds the query and passes it to the database.
     *
     * Array must be like this:
     * [
     *    'name' => 'Piper Halliwell',
     *    'AND',
     *    '>=',
     *    'id' => '2',
     * ]
     *
     * @param string $source The name of a table to move from.
     * @param string $destination The name of a table to move into.
     * @param array $filter See the description
     * @throws mysqli_sql_exception
     * @uses self::BuildCopyQuery()
     * @uses self::BuildDeleteQuery()
     * @uses self::MultiQuery()
     */
    public static function Move($source, $destination, array $filter=null)
    {
        self::Start();
        $copyQuery = self::BuildCopyQuery($source, $destination, $filter);
        $deleteQuery = self::BuildDeleteQuery($source, $filter);
        self::MultiQuery($copyQuery . ';' . $deleteQuery);
    }


    /**
     * GetLastQuery
     *
     * Returns the last performed query.
     * It's may be useful for debugging purposes.
     *
     * @return string
     * @uses self::$lastQuery
     */
    public static function GetLastQuery()
    {
        return self::$lastQuery;
    }


    /**
     * PrepareQuery
     *
     * Loads a precompiled query into the database.
     *
     * $stmt=DBManager::PrepareQuery(
     *   "SELECT \`types_of_attachments\`.\`type\`, \`content\` FROM \`attachments\` JOIN \`types_of_attachments\` ON \`attachments\`.\`type\` = \`types_of_attachments\`.\`id\` WHERE \`attachments\`.\`id\` = ?",
     *   'i',
     *   $id
     * );
     *
     * @param string $query A query with '?'s instead of variables.
     * @param string $typesMask String of count($bindings) symbols. i - integer, d - double, s - string.
     * @param mixed &$bindings Either a reference to variable which is supposed to vary or a reference to array of such variables.
     * @throws mysqli_sql_exception
     */
    public static function PrepareQuery($query, $typesMask=null, &$bindings=null)
    {
        self::Start();
        self::$lastQuery = $query;
        self::$preparedQuery = self::$mysqli->prepare($query);

        if (!empty($typesMask) && !empty($bindings))
        {
            $arr = [$typesMask];
            if (is_array($bindings))
                for ($i = 0; $i < count($bindings); $i++)
                    $arr[] = &$bindings[$i];
            else
                $arr[] = &$bindings;

            call_user_func_array(array(self::$preparedQuery, 'bind_param'), $arr);
        }
    }


    /**
     * ExecutePreparedQuery
     *
     * Executes the prepared query, that was stored before, with current values of bound variables.
     *
     * @return mysqli_result
     * @throws mysqli_sql_exception
     */
    public static function ExecutePreparedQuery()
    {
        $statement = self::$preparedQuery;

        $statement->execute();
        $result = $statement->get_result();
        $statement->close();

        return $result;
    }


    /**
     * CustomQuery
     *
     * Loads a precompiled query into the database and executes it with current values of variables.
     *
     * DBManager::CustomQuery(
     *   "INSERT INTO \`attachments\` (\`type\`, \`content\`) VALUES ((SELECT \`id\` FROM \`types_of_attachments\` WHERE \`type\` = ?), ?)",
     *   'ss',
     *   [$this->GetAttachmentType(), $this->GetAttachment()]
     * );
     *
     * @param string $query A query with '?'s instead of variables.
     * @param string $typesMask String of count($bindings) symbols. i - integer, d - double, s - string.
     * @param mixed $values Either a variable which is supposed to vary or an array of such variables.
     * @return mysqli_result
     * @throws mysqli_sql_exception
     */
    public static function CustomQuery($query, $typesMask=null, $values=null)
    {
        $bindings = $values;
        self::PrepareQuery($query, $typesMask, $bindings);
        return self::ExecutePreparedQuery();
    }


    /**
     * StartTransaction
     *
     * Begins a transaction. Call other methods to modify data in the database and use the Commit() method to save the changes if everything is OK.
     *
     * @throws mysqli_sql_exception
     */
    public static function StartTransaction()
    {
        self::Start();
        self::$committed = false;
        self::Query("START TRANSACTION;");
    }


    /**
     * Commit
     *
     * Commits a previously opened transaction into the database to save the changes.
     * Returns true if the operation has been completed successfully. Returns false if there is no started transaction. Throws an exception in case of any error.
     *
     * @return bool
     * @throws mysqli_sql_exception
     */
    public static function Commit()
    {
        if (self::$committed)
            return false;

        self::$committed = true;
        self::Query("COMMIT;");
        return true;
    }
}