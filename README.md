Simple Database Adapter (SDBA)
==============================

Для русскоговорящих
-------------------

Подробное руководство по библиотеке лежит [здесь](https://bitbucket.org/Kozalo/simpledatabaseadapter/wiki/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F%20%D0%BF%D0%BE%20SDBA%20%D0%BD%D0%B0%20%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%BE%D0%BC%20%D1%8F%D0%B7%D1%8B%D0%BA%D0%B5).  
Также его можно найти в самом репозитории тут: `docs/russian.md`.  


In English
----------

To see the documentation in English, open [this link](https://bitbucket.org/Kozalo/simpledatabaseadapter/wiki/Documentation%20for%20SDBA%20in%20English).  
Also, you can find it alongside other files using this path: `docs/english.md`.  
